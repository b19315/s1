package com.batch193.activity1;

import java.util.Scanner;

public class Activity1 {

    public static void main(String[] args){
        Scanner appScanner = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = appScanner.nextLine().trim();

        System.out.println("Last Name:");
        String lastName = appScanner.nextLine().trim();

        System.out.println("First Subject Grade:");
        double firstSubject = appScanner.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubject = appScanner.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubject = appScanner.nextDouble();

        int total = (int) ((firstSubject + secondSubject + thirdSubject)/3);

        System.out.println("Good Day," + " " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is:" + " " + total);
    }

}
